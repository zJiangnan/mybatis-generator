package com.zsh.config;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

/**
 * @Package: com.zsh.config
 * @Author: pluto
 * @CreateTime: 2021/7/4 3:05 下午
 * @Description: 生成自定义注解
 **/
public class Generator {
    private final static String tou = "jdbc:mysql://drdsbgga6ml36o99public.drds.aliyuncs.com:3306/";
    private final static String wei = "?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai";

    private final static String DEV_USERNAME = "sinopec_ops";
    private final static String DEV_PASSWORD = "sinopec_ops";
    private final static String TEST_USERNAME = "cpcadmin";
    private final static String TEST_PASSWORD = "CPC_123456";

    private static StringBuffer url = new StringBuffer();
    private String environment;
    private Properties properties = new Properties();

    public void generator() throws Exception {

        List<String> warnings = new ArrayList<String>();
        boolean overwrite = true;
        //指定 逆向工程配置文件 路径从项目名开始
        File configFile = new File("src/main/resources/generatorConfig.xml");
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
        //如果在没有生成代码，则请查看：warnings
    }

    public static void main(String[] args) throws Exception {
        try {
            Generator generatorSqlmap = new Generator();
//            generatorSqlmap.sck();
            generatorSqlmap.generator();
            System.out.println("完成😄");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sck() {
        boolean flase = true;
        do {
            environment = this.out("输入环境(dev and test):");
//            变更环境的URL
            switch (environment) {
                case "dev":
                    flase = false;
                    this.url(this.out("输入库名："));
                    this.dataIn();
                    break;
                case "test":
                    flase = false;
                    this.url(this.out("输入库名："));
                    this.dataIn();
                    break;
                default :
                    System.out.println("输入无效!");
            }
        } while (flase);
        try {
            properties.store(new FileOutputStream("src/main/resources/generator.properties"), "Mybatis-Generator");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void url(String database) {
        if (environment.equals("dev")) {
            url.append("jdbc:mysql://10.202.43.60:31893/");
            url.append(database);
            url.append("?autoReconnect=true&useSSL=false&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai");
            properties.setProperty("mysql.url", url.toString());
            properties.setProperty("mysql.username", DEV_USERNAME);
            properties.setProperty("mysql.password", DEV_PASSWORD);
        } else {
            url.append("jdbc:mysql://drdsbgga6ml36o99public.drds.aliyuncs.com:3306/");
            url.append(database);
            url.append("?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai");
            String s = "jdbc:mysql://drdsbgga6ml36o99public.drds.aliyuncs.com:3306/" + database + "?useUnicode=true&characterEncoding=UTF-8&serverTimezone" +
                    "=Asia/Shanghai";
            System.out.println(tou + database + wei);
            properties.setProperty("mysql.url", tou);
            properties.setProperty("mysql.username", TEST_USERNAME);
            properties.setProperty("mysql.password", TEST_PASSWORD);
        }
    }

    private String out(String msg) {
        Scanner sc = new Scanner(System.in);
        System.out.println(msg);
        return sc.next();
    }

    private void dataIn() {
        properties.setProperty("project.name", this.out("请输入服务名："));
        properties.setProperty("table.name", this.out("请输入表名："));
        properties.setProperty("java.entity", this.out("请输入Bean名："));
    }

}
