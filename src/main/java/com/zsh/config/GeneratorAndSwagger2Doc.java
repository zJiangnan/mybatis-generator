package com.zsh.config;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.Date;
import java.util.List;

/**
 * @Package: com.zsh.config
 * @Author: pluto
 * @CreateTime: 2021/7/4 10:27 下午
 * @Description: 重写生成Swagger类
 **/
public class GeneratorAndSwagger2Doc extends PluginAdapter {

    public boolean validate(List<String> list) {
        return true;
    }

    @Override
    public boolean modelFieldGenerated(Field field, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, ModelClassType modelClassType) {
        String classAnnotation = "@ApiModel(value=\"" + topLevelClass.getType()  + "\", description = \"\")";
        String tablename = "@TableName(value=\"" + "configure_system_options"  + "\")";
        String eq = "@EqualsAndHashCode(callSuper = false)";
        if(!topLevelClass.getAnnotations().contains(classAnnotation)) {
            topLevelClass.addAnnotation("/**");
            topLevelClass.addAnnotation(" * @Package: " + topLevelClass.getType());
            topLevelClass.addAnnotation(" * @Author: pluto");
            topLevelClass.addAnnotation(" * @CreateTime: " + new Date());
            topLevelClass.addAnnotation(" * @Description: " + topLevelClass.getType());
            topLevelClass.addAnnotation(" **/");
            topLevelClass.addAnnotation("@Data");
            topLevelClass.addAnnotation(eq);
            topLevelClass.addAnnotation(classAnnotation);
            topLevelClass.addAnnotation(tablename);
        }

        String apiModelAnnotationPackage =  properties.getProperty("apiModelAnnotationPackage");
        String apiModelPropertyAnnotationPackage = properties.getProperty("apiModelPropertyAnnotationPackage");
        if(null == apiModelAnnotationPackage) apiModelAnnotationPackage = "io.swagger.annotations.ApiModel";
        if(null == apiModelPropertyAnnotationPackage) apiModelPropertyAnnotationPackage = "io.swagger.annotations.ApiModelProperty";

        topLevelClass.addImportedType(apiModelAnnotationPackage);
        topLevelClass.addImportedType(apiModelPropertyAnnotationPackage);
        topLevelClass.addImportedType("com.baomidou.mybatisplus.annotation.TableName");
        topLevelClass.addImportedType("lombok.Data");
        topLevelClass.addImportedType("lombok.EqualsAndHashCode");
        topLevelClass.addImportedType("java.io.Serializable");

        field.addAnnotation("@ApiModelProperty(value=\"" + introspectedColumn.getRemarks() + "\")");
        return super.modelFieldGenerated(field, topLevelClass, introspectedColumn, introspectedTable, modelClassType);
    }

}
