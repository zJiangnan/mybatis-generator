# MyBatis逆向工程的项目
# 同时支持Swagger
# 注意事项
- 修改 `generator.properties` 配置文件的内容
    - `project.name`：项目名，也就是包路径名
    - `company.name`：公司名，最后的包路径为：`com.{company.name}.{project.name}`
    - `table.name`：表名
    - `java.entity`：实体类文件名
- 